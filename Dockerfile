FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest

MAINTAINER Ricardo Rocha <ricardo.rocha@cern.ch>

RUN echo $'\n\
[eos7-stable] \n\
name=EOS binaries from CERN Koji [stable] \n\
baseurl=http://linuxsoft.cern.ch/internal/repos/eos7-stable/x86_64/os \n\
enabled=1 \n\
gpgcheck=False \n\
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-koji file:///etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2 \n\
priority=1 \n'\
>> /etc/yum.repos.d/eos7-stable.repo

RUN yum install -y \
	eos-fusex \
	initscripts \
	jq \
	procps-ng \
	&& yum clean all

ADD launch.sh /launch.sh

ENTRYPOINT ["/launch.sh"]
