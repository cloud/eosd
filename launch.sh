#!/bin/bash
set -e

# launch eosxd daemons
unset KRB5CCNAME
unset X509_USER_CERT
unset X509_USER_KEY
ulimit -S -c ${DAEMON_COREFILE_LIMIT:-0}
ulimit -n 65000
test -c /dev/fuse || modprobe fuse
EOSXD_MOUNT_HEALTH_CHECK_INTERVAL=${EOSXD_MOUNT_HEALTH_CHECK_INTERVAL:-10}
echo starting eosxd

# umount and remounts one EOS fuse mount
function remount_one() {
    mnt=$1
    f="/etc/eos/fuse.${mnt}.conf"
    echo "eosxd: adding fuse mount /eos/${mnt}"
    mkdir -p /eos/${mnt} || true
    umount /eos/${mnt} || true
    /usr/bin/eosxd -o rw,fsname=${mnt} &

    letters=$(cat $f | jq -r ".bind")
    if [ "$letters" != "null" ]
    then
        echo "eosxd: adding bind mounts ${letters} for ${mnt}"
	instance=$(echo ${mnt}| cut -d- -f1)
        area=$(cat $f | jq -r ".remotemountdir")
        mkdir -p ${area} || true
        for l in $letters; do
            echo "eosxd: adding symlink for ${area}${l} in /eos/${mnt}/${l}"
            ln -s /eos/${mnt}/${l} ${area}${l} || true
            echo "eosxd: adding symlink for /eos/${instance}-${l} in /eos/${mnt}/${l} "
            ln -s /eos/${mnt}/${l} /eos/${instance}-${l} || true
        done
    fi
}

# returns the list of eosxd mounts as names like `atlas`
# (expected to be mounted at /eos/atlas)
function get_mount_list() {
    for f in $(ls /etc/eos/fuse.*.conf); do
        echo $(basename $f | sed "s/fuse\.\(.*\)\.conf/\1/")
    done
}

# Checks if any mount point needs to be restarted.
# Adapted from https://gitlab.cern.ch/ai/it-puppet-module-eosclient/-/blob/qa/code/files/eos-cleanup.sh
function check_mounts() {
    for mnt in $(get_mount_list)
    do
        EOSXD=$(pgrep -u root -f "/usr/bin/eosxd -o rw,fsname=${mnt}" || true)
        if [[ -z "${EOSXD}" ]]
        then
            echo "/eos/${mnt} should be mounted but corresponding eosxd is not running. Re-mounting."
            remount_one $mnt
        fi
    done
}

# Initial mount
for mnt in $(get_mount_list); do
    remount_one $mnt
done

while true; do
    sleep "${EOSXD_MOUNT_HEALTH_CHECK_INTERVAL}"
    check_mounts
done
