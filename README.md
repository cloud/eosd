> :warning: This repository [has moved](https://gitlab.cern.ch/kubernetes/storage/eosxd). The new location is a fork and not a repository migration so that old container registry images were kept.

# What is eosd

eosd is the fuse client for the EOS filesystem.

Check full documentation [here](http://eos.readthedocs.io/en/latest/configuration/fuse.html).

## What's in this image

This image contains the installation of eos, taken from the official rpms.

It provides an easy way to launch eosd instances, one per mount point. 

It integrates well with the [docker-volume-eos](https://gitlab.cern.ch/cloud-infrastructure/docker-volume-eos)
docker plugin in case you want to have it.

## Features

* argument to specify the mount point
* credentials shared with the host, to integrate with [docker-volume-eos](https://gitlab.cern.ch/cloud-infrastructure/docker-volume-eos)

# Usage

The image currently requires privileged mode and shared pid namespace.

Here's a sample usage of the container image:
```
docker run --name eosd --pid=host --privileged --net host \
	-v /etc/krb5.keytab:/etc/krb5.keytab \
	-v /var/run/eosd/credentials:/var/run/eosd/credentials \
	-v /eos:/eos:shared \
	-v /var/run/eosd:/var/run/eosd \
	gitlab-registry.cern.ch/cloud/eosd user
```

# Issues

# Contributing

You are invited to contribute new features and fixes.

[Check here](https://gitlab.cern.ch/cloud/eosd) for the original Dockerfile and repository.

# Authors

Ricardo Rocha [ricardo.rocha@cern.ch](mailto:ricardo.rocha@cern.ch)

